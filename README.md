# Spike Report

## SPIKE PE 7 - Cable Simulation

### Introduction

Wires and Cables are often simulated in a realistic fashion in modern games. How can Unreal Engine help us do it?


### Goals

* The Spike Report should answer each of the Gap questions

Building on Spike PE-6

* Create a new type of Target which has Cable elements
* See how you can combine a target which includes Cable, Cloth and Destructible elements


### Personnel

In this section, list the primary author of the spike report, as well as any personnel who assisted in completing the work.

* Primary - Jacob Pipers
* Secondary - N/A

### Technologies, Tools, and Resources used

Provide resource information for team members who wish to learn additional information from this spike.

* [Cable](https://docs.unrealengine.com/latest/INT/Videos/PLZlv_N0_O1ga0aV9jVqJgog0VWz1cLL5f/HNlYg2qY7cI/)
* [integration](http://physicsforidiots.com/maths/integration/)
* [Verlet Integration](https://en.wikipedia.org/wiki/Verlet_integration)
* Visual Studio
* Unreal Engine


### Tasks Undertaken

Show only the key tasks that will help the developer/reader understand what is required.

* Follow the [Cable Guide](https://docs.unrealengine.com/latest/INT/Videos/PLZlv_N0_O1ga0aV9jVqJgog0VWz1cLL5f/HNlYg2qY7cI/)

* Attach Cable to target.


### What we found out

Knowledge: What is Verlet Integration?

Integration is the  
Verlet Integration is the Calculation of trajectories of particles. 

Verlet integration is a numerical method used to integrate Newton's equations of motion.

### [Optional] Open Issues/Risks


List out the issues and risks that you have been unable to resolve at the end of the spike. You may have uncovered a whole range of new risks as well.

### [Optional] Recommendations

You may state that another spike is required to resolve new issues identified (or) indicate that this spike has increased the team�s confidence in XYZ and move on.