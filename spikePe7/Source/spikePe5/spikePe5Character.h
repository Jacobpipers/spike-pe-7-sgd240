// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/Character.h"
#include "spikePe5Character.generated.h"


#define SURFACE_Wood SurfaceType1
#define SURFACE_Stone SurfaceType2
#define SURFACE_Metal SurfaceType3

class UInputComponent;

UENUM(BlueprintType)
enum class WeaponTypeEnum : uint8
{
	None UMETA(DisplayName = "None"),
	Laser UMETA(DisplayName = "Laser"),
	Riffle UMETA(DisplayName = "Riffle"),
	ForcePush UMETA(DisplayName = "Force Push")
};
USTRUCT(BlueprintType)
struct FWeaponStruct
{
	GENERATED_BODY()
	// String which identifies our key
	FString UniqueID;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class USkeletalMesh* GunModel;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UAnimMontage* FireAnimation;

	//Get
	class USkeletalMesh* GetGunSkeleton()
	{
		return GunModel;
	}

	//Constructor
	FWeaponStruct()
	{
		//Always initialize your USTRUCT variables!
		//   exception is if you know the variable type has its own default constructor
		
		GunModel = NULL;
	}
};


UCLASS(config=Game)
class AspikePe5Character : public ACharacter
{
	GENERATED_BODY()

	/** Pawn mesh: 1st person view (arms; seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USkeletalMeshComponent* Mesh1P;

	/** Gun mesh: 1st person view (seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USkeletalMeshComponent* FP_Gun;

	/** Location on gun mesh where projectiles should spawn. */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USceneComponent* FP_MuzzleLocation;

	/** Gun mesh: VR view (attached to the VR controller directly, no arm, just the actual gun) */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USkeletalMeshComponent* VR_Gun;

	/** Location on VR gun mesh where projectiles should spawn. */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USceneComponent* VR_MuzzleLocation;

	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FirstPersonCameraComponent;

	/** Motion controller (right hand) */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UMotionControllerComponent* R_MotionController;

	/** Motion controller (left hand) */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UMotionControllerComponent* L_MotionController;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class URadialForceComponent* FirstPersonRadialForceComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class USphereComponent* ForceFieldDisplay;


public:
	AspikePe5Character();

protected:
	virtual void BeginPlay();
	virtual void Tick(float DeltaTime);

	void ForceFieldGrow(float DeltaTime);

public:
	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseLookUpRate;

	/** Gun muzzle's offset from the characters location */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	FVector GunOffset;

	/** Projectile class to spawn */
	UPROPERTY(EditDefaultsOnly, Category = Projectile)
	TSubclassOf<class AspikePe5Projectile> ProjectileClass;

	/** Sound to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	class USoundBase* FireSound;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = RayGun)
	TArray<class USoundBase*> LaserSoundList;

	class USoundBase* LaserSound;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = RayGun)
	TArray <class UMaterialInterface*> DecalMaterialList;

	class UMaterialInterface * DecalMaterial;

	/** AnimMontage to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	class UAnimMontage* FireAnimation;



	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	TMap <FString , FWeaponStruct> Guns;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	class UMaterial * ShieldMat;

	/// Laser Weapon
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Enum)
	WeaponTypeEnum CurrentWeapon;

	TSubclassOf<class UParticleSystem> Particle;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = RayGun)
	UParticleSystem* ParticleSystem;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = RayGun)
	float RayDistance;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = RayGun)
	FVector DecalSize;
	///

	/** Whether to use motion controller location for aiming. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	uint32 bUsingMotionControllers : 1;


protected:

	/** Fires a projectile. */
	void OnFire();

	void LaserBeam(UWorld *const &World);

	void BulletWeaponFire();

	void ForcePush(UWorld *const &World);

	/** Force Field Push */
	void StartRadialFire();
	void FireRadialFire();

	void EnableThrust();
	void DisableThrust();

	/** Resets HMD orientation and position in VR. */
	void OnResetVR();

	/** Handles moving forward/backward */
	void MoveForward(float Val);

	/** Handles stafing movement, left and right */
	void MoveRight(float Val);

	/**
	* Called via input to turn at a given rate.
	* @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	*/
	void TurnAtRate(float Rate);

	/**
	* Called via input to turn look up/down at a given rate.
	* @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	*/
	void LookUpAtRate(float Rate);

	struct TouchData
	{
		TouchData() { bIsPressed = false; Location = FVector::ZeroVector; }
		bool bIsPressed;
		ETouchIndex::Type FingerIndex;
		FVector Location;
		bool bMoved;
	};
	void BeginTouch(const ETouchIndex::Type FingerIndex, const FVector Location);
	void EndTouch(const ETouchIndex::Type FingerIndex, const FVector Location);
	void TouchUpdate(const ETouchIndex::Type FingerIndex, const FVector Location);
	TouchData	TouchItem;

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;
	void ChangeWeaponDown();
	void Weapon3();
	void Weapon2();
	void Weapon1();
	void ChangeWeapon();
	void ChangeToNone();
	void ChangeToForcePush();
	void ChangeToRiffle();
	void ChangeToLaser();
	// End of APawn interface

	/*
	* Configures input for touchscreen devices if there is a valid touch interface for doing so
	*
	* @param	InputComponent	The input component pointer to bind controls to
	* @returns true if touch controls were enabled.
	*/
	bool EnableTouchscreenMovement(UInputComponent* InputComponent);

	bool IsThrustEnabled = false;

	bool HasSheildGrowingStarted = false;
	bool isForceSheildIsGrowing = false;
	float ForceShieldRadius;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		float ForceShieldThrustGrowthRateMultiply;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		float ForceShieldGrowthRate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		float MaxForceShieldRadius;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		float ForcePower;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		FQuat Rot;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		FVector Box;

public:
	/** Returns Mesh1P subobject **/
	FORCEINLINE class USkeletalMeshComponent* GetMesh1P() const { return Mesh1P; }
	/** Returns FirstPersonCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetFirstPersonCameraComponent() const { return FirstPersonCameraComponent; }

};

